require('dotenv').config();
const express = require('express');
const hbs = require('express-handlebars');
const bodyParser = require('body-parser');
const app = express();
const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
        user: process.env.GMAIL_USERNAME,
        pass: process.env.GMAIL_PASSWORD
    }
});

////######### HBS SETUP ############/////
app.engine('hbs', hbs({
    extname: 'hbs',
    defaultLayout: 'layout',
    layoutsDir: __dirname + '/views/layouts',
    partialsDir: __dirname + '/views/partials'
}));
app.set('view engine', 'hbs');

//ALL STATIC
['css', 'images', 'js', 'fonts'].forEach(dir => {
    app.use(`/${dir}`, express.static(__dirname + `/static/${dir}`))
});


const jsonParser = bodyParser.json();


// GET
app.get('/', (req, res) => res.render('home'));


// POST
app.post('/api/send_mail', jsonParser, (req, res) => {
    const {name, emaild, subject, message} = req.body;
    const email = {
        from: `${name} <${emaild}>`,
        to: 'ffaffa03@gmail.com, nebiros90@mail.ru',
        subject,
        text: message
    };

    transporter.sendMail(email, (err, info) => {
        if (err) res.status(500).send('Error with mailer');
        else
            res.json({ok: 200})
    })
});


const port = process.env.PORT;
app.listen(port, () => {
    console.log(`Server up on port ${port}`)
});